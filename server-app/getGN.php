<?php
	header('Content-type: application/json');
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept');

	// var_dump(headers_list());

	// リクエストからコメ番を取得する
	$movieId = $_GET['a'];
	
	// NIJを取得する
	$json = file_get_contents("./gijiNikoru.json");
	
	// オブジェクト化
	$data = json_decode($json, true);
	
	// NIMJを取得
	$movieInfo = $data[$movieId];
	
	if (!$movieInfo) {
		// まだNIMJが存在しない場合、初期化する
		$data[$movieId] = json_decode("{}");
		
		// JSON化
		$e = json_encode($data);
		
		// 書き込み
		file_put_contents("./gijiNikoru.json", $e);
	}
	
	// JSON化
	$movieInfoJSON = json_encode($movieInfo);
	
	// NIMJを返却する
	echo($movieInfoJSON);
